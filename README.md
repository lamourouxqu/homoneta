Le projet typographique Homoneta s’active dans une fiction - Et Homo Créa la Matière - à la manière d’un mythe dans lequel on suit l’évolution d’Homo, un personnage mystique & emblématique symbolisant la condition humaine : l’aptitude à créer, former des aspirations et d’y croire. À ielle seul·e, Homo incarne l’ensemble de l'humanité & la création de la monnaie : une construction imaginaire & un système universel dont le fonctionnement opère uniquement par croyance & confiance collective.

Plusieurs notions ont guidé les recherches typographiques d'Homoneta, à commencer par la notion d’atavisme. En biologie évolutive, l’atavisme est la réapparition d’un caractère ancestral chez un·e individu·e qui ne devrait pas le posséder; un trait perdu ou transformé au cours de l’évolution. L’atavisme - ici rattaché à la typographie - peut s’apparenter à l’hybridation typographique, ce qui ouvre naturellement mes recherches vers les classifications typographiques et « les fontes dites "hybrides" à notre époque où les formes typographiques sont empruntées, modifiées et/ou renouvelées à l’infinie, et tentent de mettre à mal toute catégorie » Laurent Müller, Essai d’une refonte des classifications, essai d’une reclassification de fontes , 2018.

Homoneta est composée de caractères latins aux héritages typographiques multiples et brouille les pistes relatives à l’époque dans laquelle la·e spectateur·ice se trouve - l’incitant à se poser des questions presque existentielles telles que « qu’est-ce que je regarde ? » & « d’où je regarde ? ». Homoneta tente de voyager à travers les âges et de croiser les époques en empruntant des caractéristiques typographiques spécifiques aux typographies avec empattements & linéales et aux écritures anciennes & manuscrites. Alors que ses empattements sont formés par effet d’optique par une transition tranchée net sur le fût qui donne un effet optique arrondi, son axe de construction est quant à lui droit - à la manière d'une linéale. L’une des autres caractéristiques réside dans l’utilisation marquées des ligatures en référence à l’écriture et ouvrages manuscrit·e·s. Elles vont des classiques jusqu’aux plus ornementales et expérimentales, en passant par les inclusives. Homoneta tente d'une part de trouver son caractère mystique dans des ligatures qui paraissent comme de nouveaux graphèmes, d'autre part elle s’accompagne d’une réelle envie d’appuyer la non-binarité du personnage Homo. Certains élements sont directement liés à l’outil plume manuscrite comme les terminaisons en goutte & la languette du "e" - un emprunt typographique de l'époque moyenâgeuse détourné comme une partie atrophiée qui sert de base à la constuction des ligatures. D'autres lettres sont nettement tranchées aux attaques ce qui semble être une partie typographique qu'elles auraient perdu au fil de l’évolution. Quant aux capitales, elles sont dessinées à la manière des lettres Romaines aux terminaisons et empattements pointus. Ici les ligatures sont dessinées à la manière de monogrammes faisant réapparaître l’usage de l’ornement et, comme ceux frappés sur la monnaie, évoquent une autorité.

_______

**Homoneta Regular & Italic sous licence libre Creative Commons **

CC BY-NC-SA
https://creativecommons.org/licenses/by-nc-sa/2.0/be/
Attribution - Adaptation - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions.

Vous êtes autorisé·e à :
	
- Partager — copier, distribuer et communiquer le matériel par tous moyens et sous tous formats 
- Adapter — remixer, transformer et créer à partir du matériel 

L'Offrant·e ne peut retirer les autorisations concédées par la licence tant que vous appliquez les termes de cette licence. 

Selon les conditions suivantes :

- Attribution — Vous devez créditer l'Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l'Oeuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l'Offrant vous soutient ou soutient la façon dont vous avez utilisé son Oeuvre. 

- Pas d’Utilisation Commerciale — Vous n'êtes pas autorisé·e à faire un usage commercial de cette oeuvre, tout ou partie du matériel la composant. 
**Licence commerciale disponible sur demande à hello@quentinlamouroux.com**

- Partage dans les Mêmes Conditions — Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l'Oeuvre originale, vous devez diffuser l'Oeuvre modifiée dans les même conditions, c'est à dire avec la même licence avec laquelle l'Oeuvre originale a été diffusée. 
- Pas de restrictions complémentaires — Vous n'êtes pas autorisé à appliquer des conditions légales ou des mesures techniques qui restreindraient légalement autrui à utiliser l'Oeuvre dans les conditions décrites par la licence. 
